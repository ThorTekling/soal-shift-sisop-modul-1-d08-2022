# soal-shift-sisop-modul-1-D08-2022



### Theresia Nawangsih 5025201144
### Thoriq Fatihassalam
### Handitanto

## Kendala : 
Hanya satu orang yang mengerjakan rangkaian pratikum -> mengerjakan soal, membuat readme, commit, dll (Theresia Nawangsih/5025201144)

# Soal 1
Pada soal ini diminta untuk membuat sistem log in dan register yang disimpan dalam file user/user.txt. Pada register akan memasukkan username dan password, dilanjutkan dengan log in jika berhasil akan terdapat pilihan att dan dl. Att ialah banyaknya jumlah percobaan berhasil log in maupun tidak dari user yang sedang log in, dan dl ialah mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah yang sesuai diinputkan oleh user.

### Register
Pada register membuat file register.sh menggunakan
` nano register.sh ` dilanjutkan dengan membuat direktori 
```
if [[ ! -d "$Users" ]]
    		then
		mkdir $Users
    	fi
```

Pengecekan username, apakah username sudah berada pada user.txt menggunakan fungsi `grep`
```
if grep -q -w "$accountuser" "$Users/user.txt"
    		then
		exists="User already exists"

		echo $exists
		echo $calendar REGISTER:ERROR $exists >> $Folder/log.txt
```

Dikarenakan pada soal terdapat syarat ketentuan password yaitu Minimal 8 karakter, Memiliki minimal 1 huruf kapital dan 1 huruf kecil, Alphanumeric, dan Tidak boleh sama dengan username maka
```
elif [ $UserPassword -lt 8 ]
		 then 
		 echo "Password minimum 8 characters"
		 
	elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    		 then
	   	 echo "Password at least upper, lower and number!"


	elif [ $password ==  $accountuser ]
		 then 
		 echo "Password can't same as username"
```

- Jika menginputkan password tidak sesuai makan akan muncul pesan peringatan seperti dibawah ini
![Password error](https://drive.google.com/uc?export=view&id=17VmBqreCxaU1qSgBRjiza89IXf0nkcPE)

- lalu jika password yang diinputkan sesuai, maka akan muncul tampilan berikut
![Password sukses](https://drive.google.com/uc?export=view&id=17VKKQpxXY4PR8ePpOMMZQjFRArQWAWre)

Dilanjutkan dengan menginputkan akun user (username & password) dengan password harus
tertutup/hidden dengan menggunakan `-s`
```
printf "Enter your username "
read accountuser

printf "Enter yout password "
read  -s password
```

Terakhir, jika sudah berhasil menginputkan akun user (username dan password) maka data percobaan register akan tercatat pada log.txt dan akun user akan tersimpan di user.txt
```
echo "Registered successfully"
      		echo $calendar REGISTER:INFO User $accountuser registered successfully >> $Folder/log.txt
      		echo $accountuser $password >> $Users/user.txt 
```
- Tampilan jika berhasil menginputkan akun user (username dan password) pada registrasi
![Regis Sukses](https://drive.google.com/uc?export=view&id=17iBtVqIfoPg8Yj0JbYk32ykJXyV3K39q)

### Log in
Pada log in untuk password sama dengan register
```
fac_func_password() {
	local  UserPassword=${#password}

	if [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    		 then
	   	 echo "Password at least upper, lower and number!"


	elif [ $UserPassword -lt 8 ]
		 then
		 echo "Password minimum 8 characters"

	else fac_func_login
	fi
}
```
Dilanjutkan dengan ``fac_func_login`` yaitu pengecekan akun user pada log.txt. Jika berhasil log in, makan akan muncul 2 (dua) pilihan yaitu att (banyaknya jumlah percobaan berhasil log in maupun tidak dari user yang sedang log in) dan dl (mendownload gambar dengan jumlah yang sesuai diinputkan oleh user)

```
			if grep -q -w "$accountuser $password" "$Users"
			then
				echo "$calendar LOGIN:INFO User $accountuser logged in" >> $Log
				echo "Login success"

				printf "Enter command [dl or att]: "
				read command total
				if [[ $command == att ]]
				then
					fac_func_att
				elif [[ $command == dl ]] && [[  -n "$total" ]]
				then
					fac_func_dl_picture
				else
					echo "Command not found"
				fi

			else
				failed="Failed login attemp on user $accountuser"
				echo $failed

				echo "$calendar LOGIN:ERROR $failed" >> $Log
			fi
``` 

Jika memilih `att` maka akan masuk pada ``fac_func_att``
```
fac_func_att (){
	if [[ ! -f "$Log" ]]
	then
		echo "Log not found"
	else
		awk -v user="$accountuser" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count-1)}' $Log
	fi
}
``` 
Log in attempt username dapat dilihat pada argumen ke 5 (lima) atau 9 (sembilan) pada log.txt, Maka banyaknya attempt akan dikurangi 1 karena pada log register hanya satu kali.

- Tampilan jika memilih opsi `att`

![Attempt Sukses](https://drive.google.com/uc?export=view&id=17qxiNDb1DkhrGG6vvCGwNvWkZ7-lfujs)

Selajutnya, jika memilih opsi `dl` maka akan masuk pada `fac_func_dl_picture`
```
fac_func_dl_picture (){
	if [[ ! -f "$Folder.zip" ]]
	then
		mkdir $Folder
		count=0
		fac_func_start
	else
		fac_func_unzip
	fi
}
```
terdapat `mkdir $Folder` ialah membuat sebuah folder terlebih dahulu lalu pada.zip. Karena pada setiap mendownload foto sejumlah yang diinputkan user, maka akan tersimpan dalam .zip. Jika sudah ada pada .zip maka foto akan terdownload dengan fungsi `fac_func_start`
``` 
fac_func_unzip (){
	unzip -P $password $Folder.zip
	rm $Folder.zip

	count=$(find $Folder -type f | wc -l)
	fac_func_start
}

fac_func_start (){
		for(( i=$count+1; i<=$total+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $Folder/PIC_$i.jpg
	done

	zip -P $password -r $Folder.zip $Folder/
	rm -rf $Folder
}
```
- Tampilan proses `dl`
![Dl Sukses](https://drive.google.com/uc?export=view&id=17rbJhsDxwEtPT0t4-IYOjLc8_5CRpjPS)

Dokumentasi data
- Data user.txt
![Data User](https://drive.google.com/uc?export=view&id=185wM6hP44VD5ZpinGiMq57wc5cnDLQCU)

- Data log.txt
![Data Log](https://drive.google.com/uc?export=view&id=181LAfQqzkw55C7Fg9-WisPfvGDkFxjht)

# Soal 2
Pada soal ini, diminta untuk membuat script awk. Dengan kasus website https://dafa.info yang di hack dengan menganalisis rerata, request per jam, mengidentifikasi IP, dan daftar IP yang mengkases website pada jam 2 pagi pada tanggal 23.

### Validasi Kondisi
```
Folder=/home/theresianwg/sisop/modul1/soal2
File=$Folder/log_website_daffainfo.log
Folderweb=$Folder/forensic_log_website_daffainfo_log
```
- Tampilan `forensic_log_website_daffainfo_log` di home

![Forensic](https://drive.google.com/uc?export=view&id=19ps7k4VYWbTnA_pGUe_H_RJiZXQF6NkW)

`File` diperlukan untuk mencatat proses selama program berjalan pada sesi tertentu, jika file belum tersedia maka harus dibuat terlebih dahulu. Sebaliknya, jika jika file sudah tersedia maka sesi tersebut akan dihapus dan doganti dengan sesi yang baru. `Folder`  digunakan untuk membaca apakah direktori ada dalam storage. Untuk mengetahui apakah file ada atau tidak dapat menggunakan `-d` dan untuk membuat Folder dapat menggunakan `mkdir`
```
if [[ ! -d "$Folderweb" ]]
then
	mkdir $Folderweb
else
	rm -rf $Folderweb
	mkdir $Folderweb
```
### Rata-rata Request per Jam yang Dikirimkan Penyerang ke Website.
Menggunakan `cat $File` AWK looping dapat untuk menghitung serangan yang telah diolah melaui gsub untuk menstubtitusikan input IP yang ada menjadi format IP yang diinginkan. Program melakukan loop dalam array. Increment akan dilakukan melalui variabel hour dan addition pada `avg` sebanyak nilai array. Jika loop tersebut sudah selesai, maka terjadi averaging yang membagi variabel avg dalam jumlah hour yang di proses pada loop. Dilanjutkan dengan hasil yang berupa nilai avg akan di print dan diinputkan dalam file rata-rata.txt
```
cat $File | awk -F: '{gsub(/"/, "", $3)arr[$3]++}
	END {
		for (i in arr) {
			if (i != "Request"){
				avg+=arr[i]
			}
		}
		avg=avg/12
		printf "rata rata serangan perjam adalah sebanyak %.2f request per jam\n\n", avg
	}' >> $Folderweb/ratarata.txt
```
- Tampilan rata-rata serangan

![Rataserangan](https://drive.google.com/uc?export=view&id=19mqydT8kjmLtHR7P4ps4nPIT0wl158_G)

### IP yang Paling Banyak Melakukan Request ke Server
Menggunakan `cat $File ` AWK looping untuk mencari IP yang paling banyak mengakses server.Pada array mencari IP dan jumlah request dengan variabel `almt` ialah IP dan `max` adalah jumah request terbanyak. dilanjutkan dengan `printf`  untuk melakukan print IP nilai max yang dimasukkan dalam result.txt
```
cat $File | awk -F: '{gsub(/"/, "", $1)arr[$1]++}
	END {
	max=0
	almt=0
	 for (ip in arr) {
	   if(arr[i] > max){
		almt=ip
		max = arr[ip]
	   }
         }
		print "IP yang paling banyak mengakses server adalah: "almt"\nCounter:" max "  request\n"
	}' >> $Folderweb/result.txt
```
### Requests yang Menggunakan User-agent Curl?
Menggunakan `cat $File` AWK untuk menghitung banyak request curl sebagai user-agent. Dimana pada setiap ditemukan curl sebagai user agent makan variabel n bertambah. Hasilnya berups banyaknya request yang menggunakan curl sebagai user-agent akan di print dan diinputkan dalam file result.txt
```
cat $File | awk '/curl/ {n++} 
	END {
		print "Ada " n " request yang menggunakan curl sebagai user-agent\n"
	}' >> $folder/result.txt
```
### IP yang Mengakses Website pada Jam 2 Pagi.
Menggunakan `cat $File` AWK untuk mencari IP yang mengakses website pada jam 2 pagi dan juga di setiap 2022:02 dan dimasukkan kedalam file result.txt
```
cat $File | awk -F: '/2022:02/ {gsub(/"/, "", $1)arr[$1]++}
	END {
		for (ip in arr){
			print ip " Jam 2 Pagi\n"
		}
	}' >> $Folderweb/result.txt
```

- Tampilan IP yang melakukan request, request menggunakan user-agent curl, dan IP yang mengakses website pada jam 2 pagi

![IPreq](https://drive.google.com/uc?export=view&id=19kAJnIr5Cv-azrTwDoqrWrRR7zbw4Iyi)




