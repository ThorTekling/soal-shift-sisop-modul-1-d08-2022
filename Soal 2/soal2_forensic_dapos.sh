#!/bin/bash
Folder=/home/theresianwg/sisop/modul1/soal2
File=$Folder/log_website_daffainfo.log
Folderweb=$Folder/forensic_log_website_daffainfo_log

if [[ $(id -u) -ne 0 ]]
	then
	echo "Please run as root"
	exit 1
fi

if [[ ! -d "$Folderweb" ]]
then
	mkdir $Folderweb
else
	rm -rf $Folderweb
	mkdir $Folderweb
fi

cat $File | awk -F: '{gsub(/"/, "", $3)arr[$3]++}
	END {
		for (i in arr) {
			if (i != "Request"){
				avg+=arr[i]
			}
		}
		avg=avg/12
		printf "rata rata serangan perjam adalah sebanyak %.2f request per jam\n\n", avg
	}' >> $Folderweb/ratarata.txt


cat $File | awk -F: '{gsub(/"/, "", $1)arr[$1]++}
	END {
	max=0
	almt=0
	 for (ip in arr) {
	   if(arr[i] > max){
		almt=ip
		max = arr[ip]
	   }
         }
		print "IP yang paling banyak mengakses server adalah: "almt"\nCounter:" max "  request\n"
	}' >> $Folderweb/result.txt

cat $File | awk '/curl/ {n++} 
	END {
		print "Ada " n " request yang menggunakan curl sebagai user-agent\n"
	}' >> $folder/result.txt

cat $File | awk -F: '/2022:02/ {gsub(/"/, "", $1)arr[$1]++}
	END {
		for (ip in arr){
			print ip " Jam 2 Pagi\n"
		}
	}' >> $Folderweb/result.txt
