#!/bin/bash
fac_func_password() {
	local  UserPassword=${#password}

	if [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    		 then
	   	 echo "Password at least upper, lower and number!"


	elif [ $UserPassword -lt 8 ]
		 then
		 echo "Password minimum 8 characters"

	else fac_func_login
	fi
}

fac_func_login () {
	if [[ ! -f "$Users" ]]
	then
		echo "No user registered yet"
	else
		if grep -q -w "$accountuser" "$Users"
		then
			if grep -q -w "$accountuser $password" "$Users"
			then
				echo "$calendar LOGIN:INFO User $accountuser logged in" >> $Log
				echo "Login success"

				printf "Enter command [dl or att]: "
				read command total
				if [[ $command == att ]]
				then
					fac_func_att
				elif [[ $command == dl ]] && [[  -n "$total" ]]
				then
					fac_func_dl_picture
				else
					echo "Command not found"
				fi

			else
				failed="Failed login attemp on user $accountuser"
				echo $failed

				echo "$calendar LOGIN:ERROR $failed" >> $Log
			fi
		else
			echo "User not found, please register!"
		fi
	fi
}

fac_func_dl_picture (){
	if [[ ! -f "$Folder.zip" ]]
	then
		mkdir $Folder
		count=0
		fac_func_start
	else
		fac_func_unzip
	fi
}

fac_func_unzip (){
	unzip -P $password $Folder.zip
	rm $Folder.zip

	count=$(find $Folder -type f | wc -l)
	fac_func_start
}

fac_func_start (){
		for(( i=$count+1; i<=$total+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $Folder/PIC_$i.jpg
	done

	zip -P $password -r $Folder.zip $Folder/
	rm -rf $Folder
}

fac_func_att (){
	if [[ ! -f "$Log" ]]
	then
		echo "Log not found"
	else
		awk -v user="$accountuser" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count-1)}' $Log
	fi
}

calendar=$(date +%D)

printf "Enter your username "
read accountuser

printf "Enter your password "
read -s password


folder=$(date +%m-%d-%Y)_$accountuser
Log=/home/theresianwg/sisop/modul1/soal1/log.txt
Users=/home/theresianwg/sisop/modul1/soal1/users/user.txt

if [[ $(id -u) -ne 0 ]]
then
	echo "Please run as root"
	exit 1
else
	fac_func_password
fi
